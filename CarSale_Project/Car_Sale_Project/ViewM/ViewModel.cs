﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Popups;
using Car_Sale_Project.DbModel;
using Car_Sale_Project.Model;

namespace Car_Sale_Project.ViewM
{
    public class ViewModel
    {
        public const int CarOnPage = 4;

        public ObservableCollection<Car> Cars { get; private set; }
        public int CurrentPage { get; private set; } = 0;

        public string PageInfo => $"{(_pageCount > 0 ? CurrentPage + 1 : CurrentPage)} " +
                                  $"/ {_pageCount}";

        public int SortId = 0;
        public int MarkId = 0;
        public int CompanyId = 0;

        private int _pageCount; 

        #region Singleton

        public static ViewModel Instance { get; } = new ViewModel();

        private ViewModel() { }

        #endregion

        #region LoadPage

        public void ReloadPage()
        {
            CurrentPage = 0;
            TakeCars(SortId,MarkId,CompanyId);
        }

        public bool NextPage()
        {
            if (CurrentPage >= _pageCount - 1) return false;
            CurrentPage++;

            TakeCars(SortId, MarkId, CompanyId);
            return true;
        }

        public bool PreviousPage()
        {
            if (CurrentPage.Equals(0)) return false;
            CurrentPage--;

            TakeCars(SortId, MarkId, CompanyId);
            return true;
        }

        #endregion

        #region Operations

        public void Delete(Car car)
        {
            using (CarContext db = new CarContext())
            {
                db.Cars.Remove(car);
                db.SaveChanges();
            }
            ReloadPage();
        }

        #endregion

        private void TakeCars(int sortIndex, int markId, int companyId)
        {
            using (CarContext db = new CarContext())
            {
                // Calculate Count
                int count = db.Cars
                    .Where(car => markId < 1 || car.MarkId.Equals(markId))
                    .Count(car => companyId < 1 || car.Mark.CompanyId.Equals(companyId));
                _pageCount = (count % CarOnPage).Equals(0) ? count / 4 : count / 4 + 1;

                switch (sortIndex)
                {                   
                    case 1:
                        Cars = new ObservableCollection<Car>(db.Cars.OrderBy(c => c.Price)
                            .Where(car => markId < 1 || car.MarkId.Equals(markId))
                            .Where(car => companyId < 1 || car.Mark.CompanyId.Equals(companyId))
                            .Skip(CurrentPage * CarOnPage).Take(4));
                        break;
                    case 2:
                        Cars = new ObservableCollection<Car>(db.Cars.OrderByDescending(c => c.Price)
                            .Where(car => markId < 1 || car.MarkId.Equals(markId))
                            .Where(car => companyId < 1 || car.Mark.CompanyId.Equals(companyId))
                            .Skip(CurrentPage * CarOnPage).Take(4));
                        break;
                    default:
                        Cars = new ObservableCollection<Car>(db.Cars
                            .Where(car => markId < 1 || car.MarkId.Equals(markId))
                            .Where(car => companyId < 1 || car.Mark.CompanyId.Equals(companyId))
                            .Skip(CurrentPage * CarOnPage).Take(4));
                        break;
                }                
                TakeMarkAndCompany(db);
            }          
        }

        private void TakeMarkAndCompany(CarContext db)
        {
            for (int i = 0; i < Cars.Count; i++)
            {
                Cars[i].Mark = db.Marks.FirstOrDefault(m => m.Id.Equals(Cars[i].MarkId));
                Cars[i].Mark.Company = db.Companies.FirstOrDefault(c => c.Id.Equals(Cars[i].Mark.CompanyId));
            }
        }

        #region Global

        public static async void ShowMessage(string text, string title = "Warrning")
        {
            MessageDialog message = new MessageDialog(text)
            {
                Title = title,
            };
            await message.ShowAsync();
        }

        #endregion
    }
}
