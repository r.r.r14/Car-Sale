﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Car_Sale_Project.View;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace Car_Sale_Project
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static MainPage _instance;
        public static MainPage Instance => _instance;

        public MainPage()
        {
            this.InitializeComponent();
            _instance = this;
            FrameNavigate(typeof(MainWindow));

            SetTitleBar();
        }

        private void SetTitleBar()
        {
            //Присваиваем свой titleBar
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleAppBar);

            //Присваиваем стили кнопок управления окном
            var titleBar = ApplicationView.GetForCurrentView().TitleBar;

            //Задание цветов кнопкам управления окном
            titleBar.ButtonForegroundColor = Colors.White;
            titleBar.ButtonBackgroundColor = Color.FromArgb(0, 0, 0, 0);
            titleBar.ButtonHoverBackgroundColor = Color.FromArgb(100, 140, 140, 147);
            titleBar.ButtonPressedBackgroundColor = Color.FromArgb(165, 0, 122, 204);
            titleBar.ButtonInactiveBackgroundColor = Color.FromArgb(0, 0, 0, 0);
            titleBar.ButtonInactiveForegroundColor = Colors.White;
            //Цвет иконок при нажатии на кнопки управления окном
            titleBar.ButtonHoverForegroundColor = Color.FromArgb(100, 255, 255, 255);
            titleBar.ButtonPressedForegroundColor = Color.FromArgb(100, 255, 255, 255);
        }

        public void FrameNavigate(Type page,object parametr = null)
        {
            if (parametr != null)
                MainFrame.Navigate(page, parametr);
            else
                MainFrame.Navigate(page);
        }

        public void FrameBack()
        {
            if (MainFrame.CanGoBack)
                MainFrame.GoBack();
            else
                FrameNavigate(typeof(MainWindow));
        }
    }
}
