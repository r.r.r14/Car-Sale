﻿
namespace Car_Sale_Project.Model
{
    public class Car
    {
        public int Id { get; set; }
        public int MarkId { get; set; }
        public Mark Mark { get; set; }

        public string Name { get; set; }
        public string ImagePath { get; set; }

        public double FuelConsumption { get; set; }
        public double Price { get; set; }

        public string GetPriceCulture => Price.ToString("C");
    }
}
