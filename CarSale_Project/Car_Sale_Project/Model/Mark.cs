﻿
using System.Collections.Generic;

namespace Car_Sale_Project.Model
{
    public class Mark
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public string Name { get; set; }

        public List<Car> Cars { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            Mark mark = obj as Mark;
            if (ReferenceEquals(mark, null)) return false;
            return Equals(mark);
        }

        protected bool Equals(Mark other)
        {
            return this.Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
