﻿
using System.Collections.Generic;

namespace Car_Sale_Project.Model
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Mark> Marks { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
