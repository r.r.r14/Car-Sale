﻿using Car_Sale_Project.Model;
using Microsoft.EntityFrameworkCore;

namespace Car_Sale_Project.DbModel
{   
    public class CarContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Car> Cars { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Car.db");
        }
    }
}
