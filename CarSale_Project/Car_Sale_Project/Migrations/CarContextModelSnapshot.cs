﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Car_Sale_Project.DbModel;

namespace Car_Sale_Project.Migrations
{
    [DbContext(typeof(CarContext))]
    partial class CarContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.4");

            modelBuilder.Entity("Car_Sale_Project.Model.Car", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("FuelConsumption");

                    b.Property<string>("ImagePath");

                    b.Property<int>("MarkId");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.HasKey("Id");

                    b.HasIndex("MarkId");

                    b.ToTable("Cars");
                });

            modelBuilder.Entity("Car_Sale_Project.Model.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("Car_Sale_Project.Model.Mark", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Marks");
                });

            modelBuilder.Entity("Car_Sale_Project.Model.Car", b =>
                {
                    b.HasOne("Car_Sale_Project.Model.Mark", "Mark")
                        .WithMany("Cars")
                        .HasForeignKey("MarkId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Car_Sale_Project.Model.Mark", b =>
                {
                    b.HasOne("Car_Sale_Project.Model.Company", "Company")
                        .WithMany("Marks")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
