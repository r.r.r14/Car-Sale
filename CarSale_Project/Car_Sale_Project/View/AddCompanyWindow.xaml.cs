﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Car_Sale_Project.DbModel;
using Car_Sale_Project.Model;
using Car_Sale_Project.ViewM;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Car_Sale_Project.View
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class AddCompanyWindow : Page
    {
        private Company _company;
        private ObservableCollection<Mark> _marks;

        public AddCompanyWindow()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _company = new Company();
            _marks = new ObservableCollection<Mark>();           
            MarksBox.ItemsSource = _marks;
        }

        private void Back_OnClick(object sender, RoutedEventArgs e)
        {
            MainPage.Instance.FrameBack();
        }

        private void Create_OnClick(object sender, RoutedEventArgs e)
        {
            using (CarContext db = new CarContext())
            {
                _company.Name = CompanyName.Text;
                db.Companies.Add(_company);
                db.SaveChanges();

                //Add Marks
                foreach (var mark in _marks)
                {
                    db.Companies.Attach(_company);
                    db.Marks.Add(mark);
                    db.Update(mark.Company);

                    db.SaveChanges();
                }               
            }
            MainPage.Instance.FrameBack();
        }

        #region Marks

        private void AddMark_OnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(MarkName.Text))
            {
                ViewModel.ShowMessage("Invalid Mark Name");
                return;
            }

            _marks.Add(new Mark{Company = _company, Name = MarkName.Text});
        }

        private void EditMark_OnClick(object sender, RoutedEventArgs e)
        {
            if (MarksBox.SelectedIndex < 0)
            {
                ViewModel.ShowMessage("Select Mark");
                return;
            }

            int index = MarksBox.SelectedIndex;
            _marks[index] = new Mark { Company = _company, Name = MarkName.Text };
        }

        #endregion


        
    }
}
