﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Car_Sale_Project.DbModel;
using Car_Sale_Project.Model;
using Car_Sale_Project.ViewM;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Car_Sale_Project.View
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainWindow : Page
    {
        public List<Company> Companies;  

        public MainWindow()
        {
            this.InitializeComponent();
            this.Loaded += MainWindow_Loaded;

            //C:\Users\Lenovo\AppData\Local\Packages\0cb54cd9-9051-43b5-8a1a-4d04768d6d6f_pthpn8nb9xcaa\LocalState
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Companies = new List<Company> { new Company { Name = "None" } };
            using (CarContext db = new CarContext())
            {
                Companies.AddRange(db.Companies.ToList());
            }
            CarCompany.ItemsSource = Companies;
            CarCompany.SelectedIndex = 0;
            
            ReloadPage();
        }

        private void AddCar_OnClick(object sender, RoutedEventArgs e)
        {
            MainPage.Instance.FrameNavigate(typeof(AddCarWindow));
        }

        private void EditCar_OnClick(object sender, RoutedEventArgs e)
        {
            if (ReferenceEquals(CarsView.SelectedItem, null))
            {
                ViewModel.ShowMessage("Chose car!");
                return;
            }
            MainPage.Instance.FrameNavigate(typeof(AddCarWindow), CarsView.SelectedItem as Car);
        }

        private void RemoveCar_OnClick(object sender, RoutedEventArgs e)
        {
            Car car = CarsView.SelectedItem as Car;
            if (car == null)
            {
                ViewModel.ShowMessage("Chose car to remove!");
                return;
            }
            
            ViewModel.Instance.Delete(car);
            UpdateView();
        }

        private void NextPage_OnClick(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Instance.NextPage())
                UpdateView();
        }

        private void BackPage_OnClick(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Instance.PreviousPage())
                UpdateView();
        }

        private void ReloadPage()
        {
            ViewModel.Instance.ReloadPage();
            UpdateView();
        }

        private void UpdateView()
        {
            CarsView.ItemsSource = null;
            CarsView.ItemsSource = ViewModel.Instance.Cars;
            PageInfo.Text = ViewModel.Instance.PageInfo;
        }

        #region View

        private void SortBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.Instance.SortId = SortBox.SelectedIndex;
            ReloadPage();
        }

        private void CarCompany_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ReferenceEquals(CarCompany.SelectedItem, null)) return;

            if (CarCompany.SelectedIndex > 0)
            {
                var company = CarCompany.SelectedItem as Company;
                if (ReferenceEquals(company, null)) return;

                int companyId = company.Id;
                using (CarContext db = new CarContext())
                {
                    List<Mark> marks = new List<Mark> { new Mark { Name = "None", Id = 0} };
                    marks.AddRange(db.Marks.Where(m => m.CompanyId.Equals(companyId)));
                    CarMark.ItemsSource = marks;
                }

                CarMark.SelectedIndex = 0;
            }
            else
                CarMark.ItemsSource = null;
           
            ViewModel.Instance.CompanyId = CarCompany.SelectedIndex;
            ReloadPage();
        }

        private void CarMark_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Mark mark = CarMark.SelectedItem as Mark;
            if (mark == null) return;

            ViewModel.Instance.MarkId = mark.Id;
            ReloadPage();
        }

        #endregion
    }
}
