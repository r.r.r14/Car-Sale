﻿
using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace Car_Sale_Project.View
{
    public class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string imagePath = value.ToString();
            return GetFile(imagePath);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }

        private async Task<BitmapImage> GetFile(string imagePath)
        {
            StorageFile file;
            try
            {
                file = await StorageFile.GetFileFromPathAsync(imagePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            using (IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read))
            {
                // Set the image source to the selected bitmap 
                BitmapImage bitmapImage = new BitmapImage();
                //match the target Image.Width, not shown
                await bitmapImage.SetSourceAsync(fileStream);
                return bitmapImage;
            }         
        }
    }
}
