﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Car_Sale_Project.DbModel;
using Car_Sale_Project.Model;
using Car_Sale_Project.ViewM;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Car_Sale_Project.View
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class AddCarWindow : Page
    {
        private List<Mark> _marks;
        private Car _car;

        public AddCarWindow()
        {
            this.InitializeComponent();
            this.Loaded += AddCarWindow_Loaded;
        }

        #region Load

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (ReferenceEquals(e,null) ||
                !(e.Parameter is Car)) return;

            _car = (Car) e.Parameter;
            using (CarContext db = new CarContext())
            {
                _car.Mark = db.Marks.FirstOrDefault(m => m.Id.Equals(_car.MarkId));
            }
        }

        private void AddCarWindow_Loaded(object sender, RoutedEventArgs e)
        {
            using (CarContext db = new CarContext())
            {
                CarCompany.ItemsSource = db.Companies.ToList();
            }
            if (_car != null)
                SetLoadCar();
        }

        public void SetLoadCar()
        {
            HeaderBox.Text = "Editing Car";
            Create.Content = "Save";

            CarCompany.SelectedIndex = _car.Mark.CompanyId - 1;
            CarMark.SelectedIndex = _marks.IndexOf(_car.Mark);
            CarName.Text = _car.Name;
            CarFuelConsumption.Text = _car.FuelConsumption.ToString(CultureInfo.CurrentCulture);
            CarPrice.Text = _car.Price.ToString(CultureInfo.CurrentCulture);
        }

        #endregion

        #region Buttons

        private async void ChoseImage_OnClick(object sender, RoutedEventArgs e)
        {
            FileOpenPicker imagePicker = new FileOpenPicker();
            imagePicker.FileTypeFilter.Add(".jpg");
            imagePicker.FileTypeFilter.Add(".jpeg");
            imagePicker.FileTypeFilter.Add(".png");

            var file = await imagePicker.PickSingleFileAsync();
            if (file != null)
            {
                using (IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read))
                {
                    // Set the image source to the selected bitmap 
                    BitmapImage bitmapImage = new BitmapImage { DecodePixelWidth = 600 };
                    //match the target Image.Width, not shown
                    await bitmapImage.SetSourceAsync(fileStream);
                    CarImage.Source = bitmapImage;
                    ImagePath.Text = file.Path;
                }
                ViewModel.ShowMessage("Not working (Sad(");
            }          
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainPage.Instance.FrameBack();
        }

        private void CreateCompany_Click(object sender, RoutedEventArgs e)
        {
            MainPage.Instance.FrameNavigate(typeof(AddCompanyWindow));
        }

        private void Create_OnClick(object sender, RoutedEventArgs e)
        {
            #region CheckAndSetCar
            if (!(CarMark.SelectedItem is Mark))
            {
                ViewModel.ShowMessage("Chose car Mark!");
                return;
            }
            if (string.IsNullOrWhiteSpace(CarName.Text)
                || !double.TryParse(CarFuelConsumption.Text, out double fuelConsumption)
                || !double.TryParse(CarPrice.Text, out double price))
            {
                ViewModel.ShowMessage("Invalid car fields!");
                return;
            }

            Mark mark = (Mark) CarMark.SelectedItem;
            

            #endregion

            using (CarContext db = new CarContext())
            {
                if (_car != null)
                {
                    _car.Mark = mark;
                    _car.Name = CarName.Text;
                    _car.FuelConsumption = fuelConsumption;
                    _car.Price = price;
                    _car.ImagePath = ImagePath.Text;

                    db.Cars.Update(_car);
                }
                else
                {
                    _car = new Car
                    {
                        Mark = mark,
                        Name = CarName.Text,
                        FuelConsumption = fuelConsumption,
                        Price = price,
                        ImagePath = ImagePath.Text,
                    };
                    db.Marks.Attach(_car.Mark);
                    db.Cars.Add(_car);
                }                           
                

                db.SaveChanges();
            }
            MainPage.Instance.FrameBack(); 
        }

        #endregion

        private void CarCompany_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ReferenceEquals(CarCompany.SelectedItem, null)) return;

            var company = CarCompany.SelectedItem as Company;
            if (ReferenceEquals(company, null)) return;

            int companyId = company.Id;
            using (CarContext db = new CarContext())
            {
                _marks = db.Marks.ToList().Where(m => m.CompanyId.Equals(companyId)).ToList();               
            }
            CarMark.ItemsSource = _marks;
        }
        
    }
}
